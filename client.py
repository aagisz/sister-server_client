import socket

S = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
HOST = '192.168.56.1'
PORT = 8000

print('Client running status\nHOST: {}\nPORT: {}\n'.format(HOST, PORT))

S.connect((HOST, PORT))
S.send('Hello World!'.encode())
DATA = S.recv(1024)
print(DATA)
S.close()