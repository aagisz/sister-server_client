import socket

S = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
HOST = socket.gethostname()
PORT = 8000

print('Server running status\nHOST: {}\nPORT: {}\n'.format(HOST, PORT))

S.bind((HOST, PORT))
S.listen(5)
while True:
    CONN, ADDR = S.accept()
    print('Got Connection from {}'.format(ADDR))
    DATA = CONN.recv(1024)
    print(DATA.decode('utf-8'))
    if not DATA:
        break
    CONN.send('Thank you for connecting'.encode('utf-8'))
    CONN.close()